CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The Inline Entity Form Translation module helps the user with translation fields 'Inline entity form - Complex' widget.

 * For a full description of the module visit:
   https://www.drupal.org/project/inline_entity_form_translation
   or https://www.weebpal.com/guides/inline-entity-form-translation-documentation

 * To submit bug reports and feature suggestions, or to track changes visit:
https://www.drupal.org/project/issues/inline_entity_form_translation


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 * Install the Inline Entity Form Translation module as you would normally install a contributed Drupal module. Visit https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module.
    2. Navigate to Administration > Configuration > Regional and language > Inline Entity Form Translation Settings.
    3. Enable config inline entity form, select the fields do you want translation with Inline entity form - Complex widget. Save configuration.


MAINTAINERS
-----------

 * Hai Nguyen Ngoc (HaiNN) - https://www.drupal.org/u/hainn
 * Hoang Le Van (hoanglv) - https://www.drupal.org/u/hoanglv

Supporting organizations:
 * WeebPal - https://www.drupal.org/weebpal
 * Website - https://www.weebpal.com
 * Backend developer - levanhoang310799@gmail.com