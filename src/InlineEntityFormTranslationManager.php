<?php

namespace Drupal\inline_entity_form_translation;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Language\LanguageManagerInterface;

/**
 * Provides Inline Entity Form Translation Manager.
 */
class InlineEntityFormTranslationManager implements InlineEntityFormTranslationManagerInterface {

  /**
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * @var Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * @var Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a InlineEntityFormTranslationManagerW object.
   *
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   */
  public function __construct(LanguageManagerInterface $languageManager, ConfigFactoryInterface $configFactory, ModuleHandlerInterface $moduleHandler) {
    $this->languageManager = $languageManager;
    $this->configFactory = $configFactory;
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * {@inheritdoc}
   */
  public function translationEntity(EntityInterface &$entity) {
    if (!$this->isEnableInlineEntityFormTranslation()) return;
    $allowTranslate = $this->checkEntityAllowTranslation($entity);
    if ($allowTranslate) {
      $languageTranslations = $entity->getTranslationLanguages();
      $currentLangcode = $this->languageManager->getCurrentLanguage()->getId();
      $originalLangcode = $entity->getUntranslated()->language()->getId();
      $allowedFields = $this->getAllowedFields($entity);
      if ($originalLangcode == $currentLangcode) {
        $defaultLanguage = $entity->language()->getId();
        // unset default language.
        unset($languageTranslations[$defaultLanguage]);
        $originalNode = $entity->original;
        foreach ($allowedFields as $allowedField) {
          if (json_encode($entity->get($allowedField)->getValue()) != json_encode($originalNode->get($allowedField)->getValue())) {
            $sectionsValue = $entity->get($allowedField)->getValue();
            foreach ($languageTranslations as $langcode => $language) {
              $entity->getTranslation($langcode)->get($allowedField)->setValue($sectionsValue);
            }
          }
        }
      }
      else {
        if (!$entity->hasTranslation($currentLangcode)) {
          $values = $entity->toArray();
          $entity->addTranslation($currentLangcode, $values);
        }
      }
    }
  }

  /**
   * Check allow bundle of entity can translation.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *
   * @return bool
   */
  protected function checkEntityAllowTranslation(EntityInterface $entity) {
    $allowTranslate = FALSE;
    if (!$entity->isNew()) {
      if (in_array($entity->bundle(), $this->getAllowedBundles())) {
        return TRUE;
      }
    }
    return $allowTranslate;
  }

  /**
   * {@inheritdoc}
   */
  public function getAllowedBundles() {
    static $drupal_static_fast_allowed_bundles;
    if (!isset($drupal_static_fast_allowed_bundles)) {
      $configBundles = $this->configFactory->getEditable('inline_entity_form_translation.translation_setting');
      foreach ($configBundles->getRawData() as $key => $value) {
        if ($value) {
          $arr = explode('+', $key);
          $machine_name_config = $arr[0];
          $drupal_static_fast_allowed_bundles[] = $machine_name_config;
        }
      }
      $drupal_static_fast_allowed_bundles = array_unique($drupal_static_fast_allowed_bundles);
    }

    return $drupal_static_fast_allowed_bundles;
  }

  /**
   * {@inheritdoc}
   */
  public function getAllowedFields(EntityInterface $entity) {
    static $drupal_static_fast_allowed_fields;
    $bundle = $entity->bundle();
    if (!isset($drupal_static_fast_allowed_fields[$bundle])) {
      $configBundles = $this->configFactory->getEditable('inline_entity_form_translation.translation_setting');
      foreach ($configBundles->getRawData() as $key => $value) {
        if ($value) {
          $arr = explode('+', $key);
          $entity_field_allowed = $arr[2];
          if ($entity->hasField($entity_field_allowed)) {
            $entity_cardinality = $entity->get($entity_field_allowed)->getFieldDefinition()->get('fieldStorage')->get('cardinality');
            if ($entity_cardinality == -1 || $entity_cardinality > 1) { // check field type is reference and multiple value
              $drupal_static_fast_allowed_fields[$bundle][] = $entity_field_allowed;
            }
          }
        }
      }
    }

    return $drupal_static_fast_allowed_fields[$bundle];
  }

  /**
   * {@inheritdoc}
   */
  public function isEnableInlineEntityFormTranslation() {
    static $drupal_static_fast_config_inline_entity_form_translation;
    if (!isset($drupal_static_fast_config_inline_entity_form_translation)) {
      $config = $this->configFactory->getEditable('inline_entity_form_translation.translation_setting')->getRawData();
      $drupal_static_fast_config_inline_entity_form_translation = (bool) $config['inline_entity_form_translation'];
    }
    return $drupal_static_fast_config_inline_entity_form_translation;
  }

}