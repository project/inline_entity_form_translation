<?php

namespace Drupal\inline_entity_form_translation\Form;

use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The setting form for domain translation language limitations.
 */
class TranslationSettingForm extends ConfigFormBase {
  const SETTING = 'inline_entity_form_translation.translation_setting';

  /**
   * An array of entity bundle information.
   *
   * @var array
   */
  protected $bundles;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $bundle_info_service
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityTypeBundleInfoInterface $bundle_info_service) {
    $this->entityTypeManager = $entity_type_manager;
    $this->bundles = $bundle_info_service->getAllBundleInfo();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info')
    );
  }


  /**
   * Retrieve the form ID.
   */
  public function getFormId() {
    return 'inline_entity_form_translation_setting_form';
  }

  /**
   * Build config form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $storage = $this->entityTypeManager->getStorage('field_storage_config');
    $entity_ids = $storage->getQuery()->accessCheck(TRUE)->execute();
    $field_storages = $storage->loadMultipleOverrideFree($entity_ids);
    // Sort the entities using the entity class's sort() method.
    // See \Drupal\Core\Config\Entity\ConfigEntityBase::sort().
    uasort($field_storages, ['Drupal\field\Entity\FieldStorageConfig', 'sort']);

    $form['inline_entity_form_translation'] = [
      '#type' => 'checkbox',
      '#title' => t('Enable config inline entity form'),
      '#description' => '<strong>By checking this option, translation for inline entity form will be called.</strong><br/>Only the translatable entity reference field can be displayed here.',
      '#default_value' => $this->config(self::SETTING)->get('inline_entity_form_translation')
    ];

    $list_fields = ['inline_entity_form_translation'];
    foreach ($field_storages as $field_storage) {
      if (!in_array($field_storage->getType(), ['entity_reference', 'entity_reference_revisions'])) continue;
      $entity_type_id = $field_storage->getTargetEntityTypeId();
      $filed_storage_target_type = $field_storage->getSettings()['target_type'];
      if (empty($form[$entity_type_id])) {
        $entity_type_title = str_replace('_', ' ', $entity_type_id);
        $entity_type_title = strtoupper($entity_type_title) . ' SETTINGS';

        $form[$entity_type_id] = [
          '#type' => 'details',
          '#title' => $entity_type_title,
          '#open' => TRUE,
        ];
      }
      foreach ($field_storage->getBundles() as $bundle) {
        if (!$this->bundles[$entity_type_id][$bundle]['translatable']) {
          unset($form[$entity_type_id]);
        }
        else {
          $storage_name = $field_storage->getName();
          $form[$entity_type_id][$bundle]['title'] = [
            '#type' => 'label',
            '#title' => $this->bundles[$entity_type_id][$bundle]['label']
          ];
          $list_fields[] = $bundle . '+' . $filed_storage_target_type . '+' . $storage_name;
          $form[$entity_type_id][$bundle][$bundle . '+' . $filed_storage_target_type . '+' . $storage_name] = [
            '#type' => 'checkbox',
            '#title' => $field_storage->getName(),
            '#default_value' => $this->config(self::SETTING)->get($bundle . '+' . $filed_storage_target_type . '+' . $storage_name)
          ];
        }
      }
    }
    $form_state->set('list_field', $list_fields);
    return parent::buildForm($form, $form_state);
  }

  /**
   * Save configs.
   */
  public function submitForm(array &$form, FormStateInterface $formState) {
    $values = $formState->getValues();
    $config = $this->config(self::SETTING);
    foreach ($formState->get('list_field') as $field) {
      $config->set($field, $values[$field]);
    }
    $config->save();
  }

  /**
   * Retrieve the config names.
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTING,
    ];
  }

}
