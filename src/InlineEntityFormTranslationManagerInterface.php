<?php

namespace Drupal\inline_entity_form_translation;

use Drupal\Core\Entity\EntityInterface;

/**
 * Provides Inline Entity Form Translation Manager.
 */
interface InlineEntityFormTranslationManagerInterface {

  /**
   * Translation entity before saved.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *
   * @return  \Drupal\Core\Entity\EntityInterface
   */
  public function translationEntity(EntityInterface &$entity);

  /**
   * Get list allowed bundles settings.
   *
   * @return array
   */
  public function getAllowedBundles();

  /**
   * Return array fields allowed with bundles of entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *
   * @return array
   */
  public function getAllowedFields(EntityInterface $entity);

  /**
   * Get inline entity form translation config.
   *
   * @return bool
   */
  public function isEnableInlineEntityFormTranslation();

}